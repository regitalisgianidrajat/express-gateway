// config.js

const crypto = require('crypto')

const textParts =
    '63bb520c295d069b8e3b5d30eeaec419:24b4d75cac4cf3a20c8c4d53f77e6faf6372c8605aa4f161f6af842fdff92922'.split(
        ':'
    )

const config = {
    env : 'development',
    app: {
        port: 2020,
    },
    redis : {
        port :6379,
        host: '127.0.0.1',
        
    },
    services: {
        user_uri: `http://localhost:1010/api`,
    },
    security: {
        jwt: 'jwt_auth_secret',
        IV_KEY: crypto.randomBytes(16),
        IV_HEADER: Buffer.from(textParts.shift(), 'hex'),
        ENCRYPTION_KEY: crypto.randomBytes(32),
        ENCRYPTION_AlGORITH: 'aes-256-cbc',
        ENCRYPTION_TEXT: Buffer.from(textParts.join(':'), 'hex'),
        IV_KEY_DEFAULT : 'I8zyA4lVhMCaJ5Kg',
        SECRET_MESSAGE : 'c2VydmljZV91c2Vy'
    },
    level_log : {
        error: 0,
        warn: 1,
        info: 2,
        http: 3,
        verbose: 4,
        debug: 5,
        silly: 6
    }
}

module.exports = config
