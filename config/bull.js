const Bull = require('bull')
const config = require('./config')

const connectQueue = (name) =>  new Bull(name, {
    redis : { port: config.redis.port, host: config.redis.host, }
})

module.exports = { connectQueue }