'use strict'
const crypto = require('crypto')

const algorithm = 'aes-256-cbc'
const ENCRYPTION_KEY = crypto.randomBytes(32)

exports.index = (req, res) => {
    const service = Buffer.from('service user').toString('base64')
    console.log(service)

    var key = 'abcdefghijklmnopqrstuvwx'

    var msg = req.query.message + ' - ' + req.query.secret_key

    var encrypt = crypto.createCipheriv('des-ede3', key, '')
    var theCipher = encrypt.update(msg, 'utf8', 'base64')
    theCipher += encrypt.final('base64')

    res.send(theCipher)
}

exports.verify = (req, res) => {
    // var decrypt = crypto.createDecipheriv(algorithm, Securitykey, "");
    // var s = decrypt.update(req.header('auth-token'), 'base64', 'utf8');
    console.log(req.header('auth-token'))
    let textParts = req.header('auth-token').split(':')
    let iv = Buffer.from(textParts.shift(), 'hex')
    let encryptedText = Buffer.from(textParts.join(':'), 'hex')

    let decipher = crypto.createDecipheriv(
        algorithm,
        Buffer.from(ENCRYPTION_KEY),
        iv
    )

    let decrypted = decipher.update(encryptedText)

    decrypted = Buffer.concat([decrypted, decipher.final()])

    const result = decrypted.toString()
    res.send(result)
    // res.send(s + decrypt.final('utf8'));
}
