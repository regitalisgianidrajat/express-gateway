const redis = require('redis')
const redisPort = 6379

const client = redis.createClient(redisPort)

client.on('error', (err) => {
    console.log(err)
})

const services = [
    'service_user',
    'service_product',
    'service_point',
    'service_general',
    'gateway',
]
exports.index = () => {
    services.forEach(function (item) {
        client.set(item, Buffer.from(item).toString('base64'))
        console.log(item)
    })
}
