const redis = require('redis')
const client = redis.createClient()

exports.index = (req, res, method = 'GET', URL = 'xxxx') => {
    const message = `Requested ${method} to ${URL}`

    client.get('service_user', async (err, result) => {
        if (err) {
            return reject(err)
        }
        const secret_key = result
        const { createHmac } = await import('crypto')
        const hmac = createHmac('sha256', secret_key)
        hmac.update(message)
        res.send({
            token: hmac.digest('hex'),
        })
    })
}

exports.verify = (req, res, method = 'GET', URL = 'xxxx') => {
    const message = `Requested ${method} to ${URL}`

    client.get('service_user', async (err, result) => {
        if (err) {
            return reject(err)
        }
        const secret_key = result
        const { createHmac } = await import('crypto')
        const hmac = createHmac('sha256', secret_key)
        hmac.update(message)
        token = hmac.digest('hex')
        if (token === req.header('auth-token')) {
            res.send({ status: 200, message: 'verified !' })
        } else {
            return res.status(401).send('Invalid Token')
        }
    })
}
