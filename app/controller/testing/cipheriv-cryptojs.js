'use strict'
const CryptoJS = require('crypto-js')
const crypto = require('crypto')

const algorithm = 'aes-256-cbc'
const ENCRYPTION_KEY = crypto.randomBytes(32)

exports.index = (req, res) => {
    const data = `fetch ${req.method} to user service`;
    //use aes algorithm , mode CBC and padding Pkcs7 to encrypt security key
    const encrypted = CryptoJS.AES.encrypt(data, Buffer.from("service_user").toString('base64'), {
        iv: CryptoJS.enc.Utf8.parse('I8zyA4lVhMCaJ5Kg'),
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
    });
    //generate security key to string
    var aesEncrypted = encrypted.toString();
    res.send(aesEncrypted)
}

exports.verify = (req, res) => {
    // var decrypt = crypto.createDecipheriv(algorithm, Securitykey, "");
    // var s = decrypt.update(req.header('auth-token'), 'base64', 'utf8');
    console.log(req.header('auth-token'))
    let textParts = req.header('auth-token').split(':')
    let iv = Buffer.from(textParts.shift(), 'hex')
    let encryptedText = Buffer.from(textParts.join(':'), 'hex')

    let decipher = crypto.createDecipheriv(
        algorithm,
        Buffer.from(ENCRYPTION_KEY),
        iv
    )

    let decrypted = decipher.update(encryptedText)

    decrypted = Buffer.concat([decrypted, decipher.final()])

    const result = decrypted.toString()
    res.send(result)
    // res.send(s + decrypt.final('utf8'));
}
