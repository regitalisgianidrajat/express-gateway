'use strict'
const crypto = require('crypto')

const algorithm = 'aes-256-cbc'
const ENCRYPTION_KEY = crypto.randomBytes(32)
// Must be 256 bits (32 characters)
const IV_LENGTH = 16 // For AES, this is always 16

exports.index = (req, res) => {
    const service = Buffer.from('service user').toString('base64')
    const text = 'GET to ' + service
    // var encrypt = crypto.createCipheriv(algorithm, Securitykey, "");
    // var theCipher = encrypt.update(message, 'utf8', 'base64');
    let iv = crypto.randomBytes(IV_LENGTH)
    let cipher = crypto.createCipheriv(
        algorithm,
        Buffer.from(ENCRYPTION_KEY),
        iv
    )
    let encrypted = cipher.update(text)

    encrypted = Buffer.concat([encrypted, cipher.final()])

    const theCipher = iv.toString('hex') + ':' + encrypted.toString('hex')

    // theCipher += encrypt.final('base64');

    res.send(theCipher)
}

exports.verify = (req, res) => {
    // var decrypt = crypto.createDecipheriv(algorithm, Securitykey, "");
    // var s = decrypt.update(req.header('auth-token'), 'base64', 'utf8');
    console.log(req.header('auth-token'))
    let textParts = req.header('auth-token').split(':')
    let iv = Buffer.from(textParts.shift(), 'hex')
    let encryptedText = Buffer.from(textParts.join(':'), 'hex')

    let decipher = crypto.createDecipheriv(
        algorithm,
        Buffer.from(ENCRYPTION_KEY),
        iv
    )

    let decrypted = decipher.update(encryptedText)

    decrypted = Buffer.concat([decrypted, decipher.final()])

    const result = decrypted.toString()
    res.send(result)
    // res.send(s + decrypt.final('utf8'));
}
