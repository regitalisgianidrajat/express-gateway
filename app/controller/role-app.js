const config = require('../../config/config')
const client = require('../../config/redis')
const axios = require('axios')

exports.index = async (req, res) => {
    client.get('user_roles', async(err, user_roles) => {
        if (err) throw err;

        if (user_roles) {
            res.send({
                status: 200,
                message : 'Roles From Redis !',
                data : JSON.parse(user_roles)
            })
        } else {
            try {
                const user = await axios.get(`${config.services.user_uri}/roles`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'basic': req.basic,
                    },
                })
                res.send({
                    status: 200,
                    message : 'Roles from API !',
                    data : user.data
                })
            } catch (err) {
                res.status(500).send(err)
            }
        }
      });
}
