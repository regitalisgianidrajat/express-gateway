const jwt = require('jsonwebtoken')
const config = require('../config/config')

const verifyToken = (req, res, next) => {
    const token = req.header('authorization') || req.header('auth-token')
        
        if (!token) return res.status(403).send('A token is required for authentication')

    const bearerToken = token.split(' ')[1]

    try {
        const decoded = jwt.verify(bearerToken, config.security.jwt)
        req.token = bearerToken
        req.user = decoded
        req.basic = req.header('basic')
        
    } catch (err) {
        return res.status(401).send('Invalid Token')
    }
    return next()
}

module.exports = verifyToken
