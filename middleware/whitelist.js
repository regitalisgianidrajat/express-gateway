const cors = require('cors');

const cors_setting = (req, res, next) => {
    try {
        const cors_setting = cors();
        console.log(cors_setting)
        next()
    } catch (err) {
        return res.status(401).send('Internal server error')
    }
}

module.exports = cors_setting
