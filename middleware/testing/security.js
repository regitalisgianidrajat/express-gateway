'use strict'
const CryptoJS = require('crypto-js')

const verify = (req, res, next) => {
    try {
        var decrypted = CryptoJS.AES.decrypt(req.header('basic'), 'my-secret')
        var object = decrypted.toString(CryptoJS.enc.Utf8)
        if (!object)
            return res.status(401).send('Invalid Authorization Basic Token')

        next()
    } catch (err) {
        return res.status(401).send('Invalid Authorization Basic Token')
    }
}

module.exports = verify
