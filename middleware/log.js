const appRoot = require('app-root-path');
const winston = require('winston');

const options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true, //catch all err 
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: 'info',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

const logConfiguration = {
    transports: [
      new winston.transports.File(options.file),
      // new winston.transports.Console(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
  }


const logger = new winston.createLogger(logConfiguration);

logger.stream = {
  write: function(message) {
    logger.info(message);
  },
};

module.exports = logger;