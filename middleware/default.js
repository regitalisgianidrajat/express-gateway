const CryptoJS = require('crypto-js')
const config = require('../config/config')
const client = require('../config/redis')


const generateHmac = (req, res, next) => {
    client.get(req.body.service, (err, service_user) => {
        console.log(service_user)
        var ip = req.headers['x-real-ip'] || req.connection.remoteAddress
        var url = req.protocol + '://' + req.get('host') + req.originalUrl
        const data = `${ip} fetch ${req.method} to ${url}`
        try {
            const encrypted = CryptoJS.AES.encrypt(data, service_user, {
                iv: CryptoJS.enc.Utf8.parse(config.security.IV_KEY_DEFAULT),
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7,
            })
            req.basic = encrypted.toString()

            return next()
        }catch(err){
            return res.status(403).send('security issues')
        }
})

}

module.exports = generateHmac
