const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const cors = require('./middleware/whitelist')
const log = require('./middleware/log')
const routes = require('./routes/routes')
const createError = require('http-errors')
const config = require('./config/config')

const app = express()
app.use(express.json()) // json
//log
app.use(morgan('{"remote_addr": ":remote-addr", "remote_user": ":remote-user", "date": ":date[clf]", "method": ":method", "url": ":url", "http_version": ":http-version", "status": ":status", "result_length": ":res[content-length]", "referrer": ":referrer", "user_agent": ":user-agent", "response_time": ":response-time"}',{ stream: log.stream }))
app.use(cors) //cors
// enhance  Rest API's security
app.use(helmet())
app.use(
  helmet.expectCt(), //certificate transparency
  helmet.frameguard(), //clickjacking
  helmet.hsts(), //force https
  helmet.noSniff(),
  helmet.permittedCrossDomainPolicies()
);

app.use('/', routes)

app.use(async (req, res, next) => {
  next(createError.NotFound('This routes doesnt exist'));
})


// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next)=> {
  res.status(err.status || 500)
  res.send({
    error : {
      status: err.status || 500,
      message: err.message ,
      stack : err.stack
    }
  })
})

app.listen(config.app.port, () => console.log(`Server running on posr: http://localhost:${config.app.port}`))
