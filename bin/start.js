const app = require('../index');
const http = require('http');
const config = require('../config/config')

const server = http.createServer(app);

server.listen(config.app.port, () => console.log(`Server running on port: http://localhost:${config.app.port}`))