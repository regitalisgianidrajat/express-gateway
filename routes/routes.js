const express = require('express')
const userRoutes = require('./auth')
const roleRoutes = require('./role')
const securityRoutes = require('./security')
const redisService = require('../app/controller/testing/service-redis')
const basic = require('../middleware/default')

const router = express.Router()

router.get('/', (req, res) => res.send('welcome to express'))
router.get('/api/redis', redisService.index)
router.use('/api/users', function (req, res, next) {
    req.body.service = 'service_user'
    next();        
}, basic, userRoutes)
router.use('/api/roles', basic, function (req, res, next) {
    req.body.service = 'service_user'
    next();        
},roleRoutes)
router.use('/api/security', securityRoutes)

module.exports = router
