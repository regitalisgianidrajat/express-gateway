const express = require('express')
const securityApp = require('../app/controller/testing/hmac')
const cipherivApp = require('../app/controller/testing/cipheriv-cryptojs')

const router = express.Router()

router.get('hmac/request-token', securityApp.index)
router.get('hmac/verify', securityApp.verify)

router.get('/request-token', cipherivApp.index)
router.get('/verify', cipherivApp.verify)

module.exports = router
