const express = require('express')
const userApp = require('../app/controller/user-app')
const auth = require('../middleware/auth')

const router = express.Router()

router.get('/', auth, userApp.index)
router.post('/login', userApp.login)
router.post('/register', userApp.register)

module.exports = router
